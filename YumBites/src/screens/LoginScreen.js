import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet, ScrollView , Image, ImageBackground} from 'react-native'
import { Keyboard } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { navigate } from './RootNavigation';

class LoginScreen extends Component {
    state = {
       email: '',
       password: ''
    }    
    handleEmail = (text) => {
       this.setState({ email: text })
    }
    handlePassword = (text) => {
       this.setState({ password: text })
    }
    login = (email, pass) => {
       alert('email: ' + email + ' password: ' + pass)
    }
    
       
    render() {
       return (
           <ScrollView>
              <View>                   
              <Image source={require('../../assests/ic_food.png')}  style={styles.tinyLogo} />
              </View>
          <View style = {styles.container}>                   
             <TextInput style = {styles.input}
                underlineColorAndroid = "transparent"
                placeholder = "Email"
                placeholderTextColor = "#A9A9A9"
                autoCapitalize = "none"
                onChangeText = {this.handleEmail}/>
             
             <TextInput style = {styles.input}
                underlineColorAndroid = "transparent"
                placeholder = "Password"
                placeholderTextColor = "#A9A9A9"
                autoCapitalize = "none"
secureTextEntry = 'true'
onChangeText = {this.handlePassword}/>
             
             <TouchableOpacity
                style = {styles.loginButton}
                onPress = {
                   () => this.login(this.state.email, this.state.password)
                }>
                <Text style = {styles.loginButtonText}> Login </Text>
             </TouchableOpacity>
             <Text style= {styles.orLabel}>OR</Text>
             <TouchableOpacity
                style = {styles.signUpButton}
                onPress = {
                   () => this.props.navigation.navigate('SignupScreen')
                }>
                <Text style = {styles.loginButtonText}> Sign Up </Text>
             </TouchableOpacity>
          </View>
          </ScrollView>
       )
    }
 }
 export default LoginScreen   
 
 const styles = StyleSheet.create({
    containerAllign: {
        flex: 1,
        justifyContent: "center"
      },
    container: {
       flex: 1,
        justifyContent: "center"
    },
    input: {
       marginBottom: 15,
       marginLeft: 20,
       marginRight: 20,
       height: 50,
       borderColor: '#7a42f4',
       borderWidth: 2,
       borderRadius: 25,
       padding: 10,
       fontSize: 15
    },
    loginButton: {
       backgroundColor: '#DC143C',
       height: 50,
       justifyContent: 'center',
       alignItems: 'center',
       borderRadius: 25,
       marginLeft: 20,
       marginRight: 20,
    },
    loginButtonText:{
       color: 'white',
       fontWeight: "bold",
       fontSize: 17
    }
    ,
    signUpButton: {
      backgroundColor: '#472c6b',
      height: 50,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 25,
     //  marginBottom: 15,
      marginLeft: 20,
      marginRight: 20,
   },
    fullSize:{
        width: '50%', 
        height: '50%'
    },
    tinyLogo: {
       margin: 50,
        width: 150,
        height: 150,
       resizeMode: 'cover',
       alignSelf: 'center'   
      },
      orLabel: {
color: '#7a42f4',
fontSize: 17,
margin: 15,
alignSelf: 'center'
      }      
    })

 